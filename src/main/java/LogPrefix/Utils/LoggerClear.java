package LogPrefix.Utils;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;
import java.util.StringTokenizer;

public class LoggerClear {
    private final String extension = "java";

    public boolean ExploreInTheFolder(File file) {


        if (file.isFile()) {
            if (extension.equals(FilenameUtils.getExtension(file.getName()))) {
                System.out.println("Explores In a File :" + file.getName());
                clearLogEntries(file);
                return true;
            }

        } else {

            for (File files : file.listFiles()) {
                System.out.println("Explores In a folder :" + file.getName());
                ExploreInTheFolder(files);
            }
        }
        return false;
    }


    private boolean clearLogEntries(File vFile) {
        String loggerName = null;

        final File file = new File(vFile.getPath());
        final List<String> lines;
        try {
            lines = FileUtils.readLines(file, Charset.defaultCharset());

            for (int i = 0; i < lines.size(); ++i) {
                final String currentLine = lines.get(i);
                final String className = file.getName().replaceFirst(".java", ".class");
                if (currentLine.contains("Logger") && currentLine.contains(className)) {
                    final StringTokenizer st = new StringTokenizer(currentLine);
                    while (st.hasMoreTokens()) {
                        if (st.nextToken().equals("Logger")) {
                            loggerName = st.nextToken();
                            break;
                        }
                    }

                }
                if ((currentLine.contains(loggerName + ".info") || currentLine.contains(loggerName + ".debug") || currentLine.contains(loggerName + ".error") || currentLine.contains(loggerName + ".warn")) && currentLine.contains("\"") && currentLine.contains(vFile.getName())) {
                    final StringBuilder builder = new StringBuilder(currentLine);
                    builder.replace(currentLine.indexOf("["), currentLine.indexOf("]") + 2, "");
                    lines.set(i, builder.toString());
                }
            }
            FileUtils.writeLines(file, lines);
            return true;

        } catch (IOException e) {
            return false;
        }

    }

}
