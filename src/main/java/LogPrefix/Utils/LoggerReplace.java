package LogPrefix.Utils;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;
import java.util.StringTokenizer;

public class LoggerReplace {
    private static final String LOGGER = "Logger";

    public void ExploreInTheFolder(File file) {
        String ext = "java";

        if (file.isFile()) {
            if (ext.equals(FilenameUtils.getExtension(file.getName()))) {
                System.out.println("Explores In a File :" + file.getName());
                SearchForLogger(file);
            }

        } else {

            for (File files : file.listFiles()) {
                System.out.println("Explores In a folder :" + file.getName());
                ExploreInTheFolder(files);
            }
        }

    }

    private void SearchForLogger(File vFile) {
        String loggerName = null;

        final File file = new File(vFile.getPath());
        final List<String> linesUpdate;
        try {
            linesUpdate = FileUtils.readLines(file, Charset.defaultCharset());

            for (int i = 0; i < linesUpdate.size(); ++i) {
                String currentLine = linesUpdate.get(i);
                final String className = file.getName().replaceFirst(".java", ".class");
                if (currentLine.contains(LOGGER) && currentLine.contains(className)) {
                    final StringTokenizer st = new StringTokenizer(currentLine);
                    while (st.hasMoreTokens()) {
                        if (st.nextToken().equals(LOGGER)) {
                            loggerName = st.nextToken();
                            break;
                        }
                    }

                }
                if (currentLine.contains(loggerName + ".info") || currentLine.contains(loggerName + ".debug") || currentLine.contains(loggerName + ".error") || currentLine.contains(loggerName + ".warn")) {
                    if (!currentLine.contains(vFile.getName() + ":" + (i + 1))) {
                        if (currentLine.contains(vFile.getName())) {
                            currentLine = currentLine.replaceFirst(":\\d+", ":" + (i + 1));
                            linesUpdate.set(i, currentLine);

                        } else if (currentLine.contains("\"")) {
                            final String temp = vFile.getName() + ":" + (i + 1);
                            final String insertText = "[" + temp + "] ";
                            if (currentLine.contains(".java")) {
                                final StringBuilder builder = new StringBuilder(currentLine);
                                builder.replace(currentLine.indexOf("["), currentLine.indexOf("]") + 2, "");
                                currentLine = builder.toString();
                                currentLine = currentLine.substring(0, currentLine.indexOf("\"") + 1) + insertText + currentLine.substring(currentLine.indexOf("\"") + 1);
                                linesUpdate.set(i, currentLine);

                            } else {
                                currentLine = currentLine.substring(0, currentLine.indexOf("\"") + 1) + insertText + currentLine.substring(currentLine.indexOf("\"") + 1);
                                linesUpdate.set(i, currentLine);

                            }
                        }
                    }
                }
            }
            FileUtils.writeLines(file, linesUpdate);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
