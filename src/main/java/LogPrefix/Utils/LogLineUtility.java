package LogPrefix.Utils;


import java.io.File;

public class LogLineUtility {
    private File file;
    private final LoggerReplace lineReader = new LoggerReplace();
    private final LoggerClear loggerClear = new LoggerClear();

    public LogLineUtility() {
        file = new File("./src");

    }

    public void writeLines() {
        lineReader.ExploreInTheFolder(this.file);
    }

    public void clearLines() {
        loggerClear.ExploreInTheFolder(this.file);
    }


}
