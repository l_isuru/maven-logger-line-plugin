package LogPrefix;


import LogPrefix.Utils.LogLineUtility;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;

@Mojo(name = "replace", defaultPhase = LifecyclePhase.GENERATE_RESOURCES)
public class ReplaceLogs extends AbstractMojo {

    public void execute() throws MojoExecutionException, MojoFailureException {
        LogLineUtility logLineUtility = new LogLineUtility();
        logLineUtility.writeLines();
    }
}
