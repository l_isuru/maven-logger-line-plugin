# maven-logger-line-plugin



This plugin can append the class name and the line number to all the message strings, of the `Log4j` logging methods. Log changes happens in the `generate-resources` phase in the default life cycle.

To use the plugin add following plugin to the `pom.xml`


If you want to add log tags use the following configuration.

```xml

    <build>
        <plugins>
            <plugin>
                <groupId>com.dfn.plugins</groupId>
                <artifactId>log-prefix-manager</artifactId>
                <version>2.0</version>
                <executions>
                    <execution>
                        <goals>
                            <goal>replace</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>

```
If you want to remove log tags from the message or don't want to add the tag use the following configuration.

```xml

    <build>
        <plugins>
            <plugin>
                <groupId>com.dfn.plugins</groupId>
                <artifactId>log-prefix-manager</artifactId>
                <version>2.0</version>
                <executions>
                    <execution>
                        <goals>
                            <goal>clear</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>

```